using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBulletAttack : State
{
    [SerializeField] private float rotateSpeed;
    [SerializeField] private float minAngle, maxAngle;
    [SerializeField] private float fireRate;
    [SerializeField] private float duration;
    [SerializeField] private GameObject bulletGimbal;
    [SerializeField] private ProjectileManager projectileManager;
    [SerializeField] private GameObject bulletHead;
    private float curCooldown;
    private float curDuration;

    private bool setup;   

    public override void RunCurrentState()
    {
        if (!setup) return;

        float _rotateSpeed = rotateSpeed;

        if(bulletGimbal.transform.rotation.z >= maxAngle)
        {
            _rotateSpeed = rotateSpeed;
        }
        else if (bulletGimbal.transform.rotation.z <= maxAngle)
        {
            _rotateSpeed = -rotateSpeed;
        }

        bulletGimbal.transform.Rotate(new Vector3(0, 0, _rotateSpeed) * Time.deltaTime, Space.World);

        curDuration -= Time.deltaTime;
        curCooldown -= Time.deltaTime;

        if(curCooldown <= 0)
        {
            ShootData shootData = new ShootData(-bulletGimbal.transform.right,Vector2.zero);
            ProjectileManager projectile = Instantiate(projectileManager, bulletGimbal.transform.position, Quaternion.identity);
            projectile.OnSpawn(shootData);

            curCooldown = fireRate;
        }

        if(curDuration <= 0)
        {
            stateManager.SwitchState(typeof(BossRandomizer));
        }
    }

    public override void OnEnterState()
    {
        curDuration = duration;
        curCooldown = 0;

        Sequence sequence = DOTween.Sequence();
        sequence.Append(bulletHead.transform.DOLocalMoveX(0, 1f));
        sequence.AppendCallback(() => { setup = true; });
    }

    public override void OnExitState()
    {
        setup = false;
        bulletHead.transform.DOLocalMoveX(9, 1f);
    }

    private void OnDrawGizmos()
    {
        
    }
}
