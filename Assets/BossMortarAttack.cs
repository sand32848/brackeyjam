using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class BossMortarAttack : State
{
    [SerializeField] private ProjectileManager mortar;
    [SerializeField] private List<Transform> FirstAttackPoint;
    [SerializeField] private List<Transform> SecondAttackPoint;
    [SerializeField] private float interval;
    [SerializeField] private Transform mortarHead;

    private bool setup;

    public override void OnEnterState()
    {
        ActivateMortar();
    }

    public void ActivateMortar()
    {
        Sequence sequence = DOTween.Sequence();

        sequence.Append(mortarHead.transform.DOLocalMoveX(2,1f));

        sequence.AppendCallback(() =>
        {
            for (int i = 0; i < FirstAttackPoint.Count; i++)
            {
                ProjectileManager projectile = Instantiate(mortar,mortarHead.position,Quaternion.identity);

                ShootData shootData = new ShootData(Vector2.zero, FirstAttackPoint[i].position);

                projectile.OnSpawn(shootData);
            }
        }).AppendInterval(3f);

        sequence.AppendCallback(() =>
        {
            for (int i = 0; i < FirstAttackPoint.Count; i++)
            {
                ProjectileManager projectile = Instantiate(mortar, mortarHead.position, Quaternion.identity);

                ShootData shootData = new ShootData(Vector2.zero, SecondAttackPoint[i].position);

                projectile.OnSpawn(shootData);
            }
        }).AppendInterval(3f);

        sequence.AppendCallback(() => { stateManager.SwitchState(typeof(BossRandomizer)); });

    }

    public override void OnExitState()
    {
        mortarHead.transform.DOLocalMoveX(9,1f);
    }

    public override void RunCurrentState()
    {
        
    }
}
