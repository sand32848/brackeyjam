using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossIntroState : State
{
    public override void RunCurrentState()
    {

    }

    public override void OnEnterState()
    {
        Sequence sequence = DOTween.Sequence();
        sequence.Append(transform.DOMoveX(5.5f, 1f));
        sequence.AppendCallback(() => { AudioManager.Instance.PlayMusic("BossTheme"); });
        sequence.AppendCallback(() => { stateManager.SwitchState(typeof(BossRandomizer)); });

        WaveManager.checkpointEnable = true;

    }
}
