using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnModule : MonoBehaviour
{
    [SerializeField] private GameObject objectToSpawn;
    public void SpawnObject()
    {
        Instantiate(objectToSpawn,transform.position,objectToSpawn.transform.rotation);
    }
}
