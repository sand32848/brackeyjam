using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mortar : ProjectileManager
{
    [SerializeField] private GameObject explosionParticle;

    public override void OnSpawn(ShootData shootData)
    {
        GameObject player = HelperScript.GetPlayer();

        transform.DOJump(shootData.destination,8,1,2).SetEase(Ease.Linear).OnComplete(Explode);
    }

    public override void OnHit(Collider2D collider)
    {

        if (collider.tag != "Player") return;

        Explode();
    }

    public void Explode()
    {
        Destroy(gameObject);
        Instantiate(explosionParticle,transform.position,Quaternion.identity);
    }
}
