using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private PlayerMovementPrototype movementPrototype;
    [SerializeField] private GameObject player;
    [SerializeField] private WaveManager waveManager;
    void Start()
    {
        AudioManager.Instance.PlayMusic("GameplayTheme");

        Sequence sequence = DOTween.Sequence();
        sequence.Append(player.transform.DOMoveY(-3.6f, 1f).SetEase(Ease.Linear));
        sequence.AppendCallback(() => { movementPrototype.enabled = true; }).AppendInterval(1f);
        sequence.AppendCallback(() => { waveManager.enabled = true; });
    }

 
}
