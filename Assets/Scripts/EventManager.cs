using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;

public class EventManager : BrainEvent
{
    [SerializeField] private List<EventSetting> currentEventList;

    public bool WaveComplete()
    {
        if (currentEventList.All(x => x.Complete))
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public async void ActivateEvent()
    {
        for (int i = 0; i < currentEventList.Count; i++)
        {
            if (currentEventList[i].eventType == EventType.OnComplete)
            {
                await currentEventList[i].timerDelay();
                await currentEventList[i].brainEvent.asyncEvent(currentEventList[i].eventParam);

                currentEventList[i].Complete = true;
            }
            else
            {
                currentEventList[i].brainEvent.asyncEvent(currentEventList[i].eventParam);
            }
        }
    }


}

