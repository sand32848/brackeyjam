using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionModule : MonoBehaviour
{
    [SerializeField] private float damage;
    [SerializeField] private float explosionRadius;
    [SerializeField] private bool OnStart;

    private void Start()
    {
        if (OnStart)
        {
            AttackData attackData = new AttackData();

            Explode(attackData);
        }
    }

    public void Explode(AttackData attackData)
    {
       Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, explosionRadius);

        for(int i = 0; i < collider.Length; i++)
        {
           if(collider[i].transform.TryGetComponent(out HitReciever hit))
            {
                hit.InvokeOnHit(attackData);
            }

        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }

}
