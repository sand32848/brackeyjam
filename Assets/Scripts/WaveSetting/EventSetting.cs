using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

[System.Serializable]
public class EventSetting
{
    [SerializeField] public EventType eventType = EventType.With;
    [SerializeField] public float delay;
    [SerializeField] public BrainEvent brainEvent;
    [SerializeField] public List<string> eventParam;

    public bool Complete;

    public async Task timerDelay()
    {
        float time = Time.time + delay;
        while(Time.time <= time)
        {
            await Task.Yield();
        }
    }
}

public enum EventType { OnComplete,With}
