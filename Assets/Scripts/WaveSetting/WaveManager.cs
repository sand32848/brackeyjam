using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    [SerializeField] private List<EventManager> waveList;
    [SerializeField] private List<EventManager> checkPointWave;
    public static int waveIndex = 0;
    public static int checkPointIndex = 0;
    public static bool checkpointEnable;

    private void Start()
    {
        waveIndex = 0;

        if (!checkpointEnable)
        {
            waveList[waveIndex].gameObject.SetActive(true);
            waveList[waveIndex].ActivateEvent();
        }
        else
        {
            loadCheckPoint();
        }
     
    }

    private void Update()
    {
        if (waveIndex >= waveList.Count) return;

        if (waveList[waveIndex].WaveComplete())
        {
            ActivateNextWave();
        }
    }

    public void ActivateNextWave()
    {
        waveList[waveIndex].gameObject.SetActive(false);

        if (checkPointWave.Find(x => x == waveList[waveIndex]))
        {
            checkPointIndex = waveIndex;

            print(checkPointIndex);
        }

        waveIndex += 1;



        if (waveIndex >= waveList.Count) return;

        waveList[waveIndex].gameObject.SetActive(true);
        waveList[waveIndex].ActivateEvent();
    }

    public void loadCheckPoint()
    {
        waveList[checkPointIndex].ActivateEvent();
        waveIndex = checkPointIndex;
    }
}
