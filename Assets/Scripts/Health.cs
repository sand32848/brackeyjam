using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class Health : MonoBehaviour
{
    [SerializeField] public int health;
    [SerializeField] public int maxHealth;
    public Action OnDeath;
    public UnityEvent DeathEvent;
    public Action OnHealthChange;
    public UnityEvent OnHealthDamage;
    public bool multiDeathTrigger;

    protected bool Triggered;

    void Start()
    {
        health = maxHealth;
    }

    public virtual void ReduceHealth(AttackData attackData)
    {
        health -= attackData.damage;
        health = Mathf.Clamp(health, 0, maxHealth);

        OnHealthDamage.Invoke();

        if (health <= 0)
        {
            if (Triggered && !multiDeathTrigger) return;
            OnDeath?.Invoke();
            DeathEvent.Invoke();
            Triggered = true;
        }
    }

    public virtual void ReduceHealth(int damage)
    {
        health -= damage;
        health = Mathf.Clamp(health, 0, maxHealth);

        OnHealthDamage.Invoke();

        if (health <= 0)
        {
            if (Triggered && !multiDeathTrigger) return;
            OnDeath?.Invoke();
            DeathEvent.Invoke();
            Triggered = true;
        }
    }

    public virtual void RestoreHealth(int heal)
    {
        health += heal;

        OnHealthDamage.Invoke();

        health = Mathf.Clamp(health, 0, maxHealth);
    }

    public void TakeDamage(int Damage)
    {
        health -= Damage;

        if (health <= 0)
        {
            
           // GetComponent<Bandit>().enabled = false;
        }
    }
}
