using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpawner : MonoBehaviour
{
    [SerializeField] private ProjectileManager projectileManager;
    public void SpawnProjectile(ShootData shootData)
    {
        ProjectileManager projectile = Instantiate(projectileManager, transform.position, Quaternion.identity);
        projectile.OnSpawn(shootData);
    }
}
