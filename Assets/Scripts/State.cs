using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State : MonoBehaviour
{
    [SerializeField]protected StateManager stateManager;
    public abstract void RunCurrentState();
    public virtual void OnEnterState() { }
    public virtual void OnExitState() { }

    public void SetStateMachine(StateManager _statManager)
    {
        stateManager = _statManager;
    }
}
