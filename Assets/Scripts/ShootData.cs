using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootData
{
     public Vector2 ShootAngle;
    public Vector2 destination;

    public ShootData(Vector2 shootAngle, Vector2 destination)
    {
        ShootAngle = shootAngle;
        this.destination = (Vector2)destination;
    }
}
