using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProjectileManager : MonoBehaviour
{
    [SerializeField] protected int damage;
    [SerializeField] protected float launchForce;
    [SerializeField] protected UnityEvent OnHitEvent;
    protected Rigidbody2D rigidbody2D;

    private void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    public virtual void OnSpawn(ShootData shootData)
    {
        rigidbody2D.AddForce(shootData.ShootAngle * launchForce,ForceMode2D.Impulse);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        OnHit(collision);
    }

    public virtual void OnHit(Collider2D collider)
    {
        OnHitEvent.Invoke();

        AttackData attackData = new AttackData(damage);

        if(collider.transform.TryGetComponent(out HitReciever onHit))
        {
            onHit.InvokeOnHit(attackData);
        }
    }
}
