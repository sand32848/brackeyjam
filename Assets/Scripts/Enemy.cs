using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public Animator anim;
    public GameObject enemy;

    public int maxHealth = 100;
    int currentHealth;
    public int Damage;
    public Health playerHealth;

    public Transform player;
    public bool IsFlipped;
    public Vector3 attackOffset;
    public float attackRange = 1f;
    public LayerMask attackMask;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    void Update()
    {
        
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;


        if(currentHealth <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        Debug.Log("DIE");

        enemy.SetActive(false);


        this.enabled = false;
    }


    public void LookAtPlayer()
    {
        Vector3 flipped = transform.localScale;
        flipped.z *= -1f;

        if(transform.position.x > player.position.x && IsFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            IsFlipped = false;
        }
        else if (transform.position.x < player.position.x && !IsFlipped)
        {
            transform.localScale = flipped;
            transform.Rotate(0f, 180f, 0f);
            IsFlipped = true;
        }
    }

    private void Attack()
    {
        Vector3 pos = transform.position;
        pos += transform.right * attackOffset.x;
        pos += transform.up * attackOffset.y;

        Collider2D collinfo = Physics2D.OverlapCircle(pos, attackRange, attackMask);
        if(collinfo != null)
        {
            collinfo.GetComponent<Health>().TakeDamage(Damage);
        }
    }

    void OnDrawGizmosSelected()
    {
        Vector3 pos = transform.position;
        pos += transform.right * attackOffset.x;
        pos += transform.up * attackOffset.y;

        Gizmos.DrawWireSphere(pos, attackRange);
    }
}
