using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    [SerializeField] private float cooldown;
    [SerializeField] private float minCooldown, maxCooldown;
    [SerializeField] private bool onStart;
    [SerializeField] private bool loop;
    [SerializeField] private bool randomize;
    [SerializeField] private UnityEvent OnTimerEvent;

    private bool active;
    private float curCooldown;

    public void activateTimer()
    {
        active = true;
    }

    private void Start()
    {
        if (randomize) randomizeCooldown();

        curCooldown = cooldown;

        if (onStart)
        {
            active = true;
        }
    }

    private void Update()
    {
        if (!active) return;

        curCooldown -= Time.deltaTime;

        if(curCooldown <= 0 && active)
        {
            if (!loop)
            {
                active = false;
            }
            resetCooldown();
            OnTimerEvent?.Invoke();
        }
    }

    public void deactivateTimer()
    {
        active = false;
    }

    public void resetCooldown()
    {
        curCooldown = cooldown;
    }

    public void randomizeCooldown()
    {
        curCooldown = UnityEngine.Random.Range(minCooldown, maxCooldown);
    }
}
