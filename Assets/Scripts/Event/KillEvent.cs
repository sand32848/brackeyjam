using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class KillEvent : BrainEvent
{
    [SerializeField] private List<Health> enemyList;
    private int enemyCount;
    [SerializeField] private UnityEvent OnKillComplete;
    private int killCount;

    private void Start()
    {
        for (int i = 0; i < enemyList.Count; i++)
        {
            enemyCount += 1;
        }
    }

    private void OnEnable()
    {
        for (int i = 0; i < enemyList.Count; i++)
        {
            enemyList[i].OnDeath += updateEnemyCount;
        }
    }

    public void updateEnemyCount()
    {
        killCount += 1;

        if(killCount >= enemyCount)
        {
            OnKillComplete.Invoke();
        }
    }

    public override async Task asyncEvent(List<string> eventParam = null)
    {
        while(killCount < enemyCount)
        {
            await Task.Yield();
        }
    }

   
   
}
