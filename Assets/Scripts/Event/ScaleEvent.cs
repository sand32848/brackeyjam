using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using System.Threading.Tasks;

public class ScaleEvent : BrainEvent
{
    [SerializeField] private float resetScale;
    [SerializeField] private float targetScale;
    [SerializeField] private float duration;
    [SerializeField] private bool loop;
    [SerializeField] private bool onStart;
    [SerializeField] private Ease easeType;
    [SerializeField] private LoopType loopType = LoopType.Restart;
    [SerializeField] private UnityEvent onCompleteEvent;

    private void Start()
    {
        if (!onStart) return;

        if (loop)
        {
            transform.DOScale(targetScale, duration).OnComplete(() => { onCompleteEvent.Invoke(); }).SetLoops(-1,loopType).SetEase(easeType);
        }
        else
        {
            transform.DOScale(targetScale, duration).OnComplete(() => { onCompleteEvent.Invoke(); }).SetEase(easeType);
        }
    }

    public void ResetScale()
    {
        transform.localScale = new Vector3(resetScale, resetScale, resetScale);
    }

    public override Task asyncEvent(List<string> eventParam = null)
    {
        return transform.DOScale(targetScale, duration).OnComplete(() => { onCompleteEvent.Invoke(); }).AsyncWaitForCompletion();
    }
}
