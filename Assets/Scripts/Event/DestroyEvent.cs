using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class DestroyEvent : BrainEvent
{
    public override Task asyncEvent(List<string> eventParam)
    {
        Destroy(gameObject);

        return null;
    }

    public void DestoyGameobject()
    {
        Destroy(gameObject);

    }
}
