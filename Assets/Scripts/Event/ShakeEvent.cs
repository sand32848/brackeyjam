using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class ShakeEvent : BrainEvent
{
    [SerializeField] private GameObject objectToShake;
    [SerializeField] private float duration;
    [SerializeField] private float strength;
    [SerializeField] private int vibrato;
    [SerializeField] private float randomness;
    private Tween tween;

    public void Shake()
    {
        if (tween != null)
        {
            if (tween.IsPlaying()) return;
        }
       

        if (objectToShake)
        {
            tween = objectToShake.transform.DOShakePosition(duration, strength, vibrato, randomness, false, true, ShakeRandomnessMode.Harmonic);
        }
        else
        {
            tween = transform.DOShakePosition(duration, strength, vibrato, randomness, false, true, ShakeRandomnessMode.Harmonic);
        }
       
    }


    public override async Task asyncEvent(List<string> eventParam)
    {
        if (objectToShake)
        {
           await objectToShake.transform.DOShakePosition(duration, strength, vibrato, randomness, false, true, ShakeRandomnessMode.Harmonic).AsyncWaitForCompletion();
        }
        else
        {
          await  transform.DOShakePosition(duration, strength, vibrato, randomness, false, true, ShakeRandomnessMode.Harmonic).AsyncWaitForCompletion();
        }
    }
}
