using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class BrainEvent : MonoBehaviour
{
    public virtual async Task asyncEvent(List<string> eventParam = null)
    {
        await Task.Yield();
    }
}
