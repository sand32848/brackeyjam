using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.Events;

public class ActiveEvent : BrainEvent
{
    [SerializeField] private List<GameObject> activeObject;
    [SerializeField] private UnityEvent onActiveEvent;

    public void SetObject()
    {
       
    }

    public override async Task asyncEvent(List<string> eventParam = null)
    {
        bool active = eventParam[0] == "active" ? true : false;

        if (active)
        {
            onActiveEvent.Invoke();
        }

        activeObject.ForEach(x => x.SetActive(active));

        await Task.Yield();
    }
}
