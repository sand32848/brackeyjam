using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using System.Threading.Tasks;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class MoveEvent : BrainEvent
{
    public MoveParam moveParam;

    private void Start()
    {
        if (moveParam.onStart)
        {
            Move();
        }
    }

    public Tween Move()
    {
        int loop = moveParam.loop == true ? -1 : 0;

        return moveParam.objectTransform.DOLocalMove(moveParam.position, moveParam.duration).SetEase(moveParam.easeType).SetUpdate(UpdateType.Fixed).OnComplete(() => { moveParam.onMoveComplete.Invoke(); }).SetLoops(loop);
    }

    public override Task asyncEvent(List<string> eventParam = null)
    {
        return Move().AsyncWaitForCompletion();
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = Color.green;

        if (moveParam.objectTransform.parent)
        {
            Handles.DrawAAPolyLine(5f, moveParam.objectTransform.position, moveParam.objectTransform.parent.TransformPoint(moveParam.position) );
        }
        else if (moveParam.objectTransform)
        {
            Handles.DrawAAPolyLine(5f, moveParam.objectTransform.position, moveParam.position);
        }
        else
        {
            Handles.DrawAAPolyLine(5f, transform.position, moveParam.position);
        }
    }

    private void GizmoDrawLine()
    {
        Handles.color = Color.green;
        Handles.DrawAAPolyLine(5f, transform.position, moveParam.position);
    }
#endif
}

[System.Serializable]
public class MoveParam
{
    public bool onStart = true;
    public bool loop = false;
    public float duration;
    public Transform objectTransform;
    public Ease easeType;
    public Vector3 position = new Vector3(0,0,0);
    public UnityEvent onMoveComplete;
}
