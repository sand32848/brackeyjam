using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class FadeEvent : BrainEvent
{
    private CanvasGroup fadeCanvasGroup => GameObject.FindGameObjectWithTag("FadeCanvas").GetComponent<CanvasGroup>();
    private float alphaValue;
    private float fadeTime;
    private Ease ease;
    
    public override async Task asyncEvent(List<string> eventParam)
    {
        await fadeCanvasGroup.DOFade(float.Parse(eventParam[0]), float.Parse(eventParam[1])).SetEase(ease).AsyncWaitForCompletion(); 
    }
}
