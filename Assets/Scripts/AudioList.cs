using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "AudioList", menuName = "ScriptableObjects/AudioList", order = 1)]
public class AudioList : ScriptableObject
{
    public Sound[] soundList;
}
