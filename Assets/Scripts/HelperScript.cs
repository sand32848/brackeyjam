using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelperScript : MonoBehaviour
{
    public static GameObject GetPlayer()
    {
        return GameObject.FindGameObjectWithTag("Player");
    }

    public static Vector2 GetPlayerPosition()
    {
        return GameObject.FindGameObjectWithTag("Player").transform.position;
    }

    public static Vector2 GetAngleToPlayer(Vector2 originPosition)
    {
        return  ((Vector2)GetPlayerPosition() - originPosition).normalized;
    }

    public static Vector2 GetAngleFromPlayer(Vector2 originPosition)
    {
        return (originPosition - (Vector2)GetPlayerPosition()).normalized;
    }

    public static Quaternion RotateTo(Transform target, Transform origin,float rotateSpeed)
    {
        Vector2 Direction = (target.position - origin.position).normalized;
        var angle = Mathf.Atan2(Direction.y, Direction.x) * Mathf.Rad2Deg;
        var angleCal = Quaternion.AngleAxis(angle, Vector3.forward);
        var step = rotateSpeed * Time.deltaTime;
        return Quaternion.RotateTowards(origin.rotation, angleCal, step);
    }

    public static Quaternion RotateDirection(Vector2 direction, Transform origin, float rotateSpeed)
    {
        var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        var angleCal = Quaternion.AngleAxis(angle, Vector3.forward);
        var step = rotateSpeed * Time.deltaTime;
        return Quaternion.RotateTowards(origin.rotation, angleCal, step);
    }
}
