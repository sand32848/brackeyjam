using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExploderEnemy : State
{
    [SerializeField] private int damage;
    [SerializeField] private float speed;
    [SerializeField] private float explosionRadius;
    [SerializeField] private Transform gimball;
    [SerializeField] private GameObject explosion;
    [SerializeField] private Health health;
    private Rigidbody2D rigidbody2D;

    private void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    public override void RunCurrentState()
    {
        gimball.rotation = HelperScript.RotateTo(HelperScript.GetPlayer().transform, gimball, 9999);
        rigidbody2D.velocity = gimball.right * speed;

        if(Vector2.Distance(transform.position,HelperScript.GetPlayerPosition()) <= explosionRadius)
        {
            health.ReduceHealth(9999);
        }
    }

    public void Explode()
    {
        Instantiate(explosion, transform.position, Quaternion.identity);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position,explosionRadius);
    }
}
