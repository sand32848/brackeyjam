using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using System;
using UnityEngine.SceneManagement;

public class GameOverUI : MonoBehaviour
{
    [SerializeField] private GameObject gameOverUI;

    private void OnEnable()
    {
        PlayerHealth.OnPlayerDeath += displayGameOver;
    }

    private void OnDisable()
    {
        PlayerHealth.OnPlayerDeath -= displayGameOver;
    }

    public void displayGameOver()
    {
        Sequence sequence = DOTween.Sequence();
        sequence.AppendInterval(0.5f);
        sequence.Append(gameOverUI.GetComponent<RectTransform>().DOAnchorPosY(67, 1f));
    }


}
