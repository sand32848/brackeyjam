using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackData
{
    public int damage;
    
    public AttackData(int damage = 1)
    {
        this.damage = damage;
    }
}
