using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : Health
{
    [SerializeField] private float invulTime;
    private bool invul;
    private float curInvulTime;
    public bool death;

    public static Action OnPlayerDeath;
    public static Action<int> OnPlayerDamage;

    private void Start()
    {
        curInvulTime = invulTime;
    }

    private void Update()
    {
        if (!invul) return;

        curInvulTime -= Time.deltaTime;

        if(curInvulTime <= 0)
        {
            invul = false;
            curInvulTime = invulTime;
        }
    }

    public void resumeHealth()
    {
        health = 3;
        death = false;
        OnPlayerDamage?.Invoke(health);
    }

    public override void ReduceHealth(AttackData attackData)
    {
        if (invul) return;
        if (death) return;

        invul = true;

        health -= 1;

        health = Mathf.Clamp(health, 0, maxHealth);

        OnPlayerDamage?.Invoke(health);

        if (health <= 0)
        {
            death = true;
            OnPlayerDeath?.Invoke();
            DeathEvent?.Invoke();
        }

        AudioManager.Instance.Play("Hurt");
    }

}
