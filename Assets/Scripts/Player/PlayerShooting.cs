using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerShooting : MonoBehaviour
{
    [SerializeField] private ProjectileManager projectile;
    [SerializeField] private float shootCooldown;
     private float curCooldown;

    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Control.Shoot.started += SpawnProjectile;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Control.Shoot.started -= SpawnProjectile;
    }

    private void Update()
    {
        curCooldown -= Time.deltaTime;
    }

    public void SpawnProjectile(InputAction.CallbackContext context)
    {
        if (curCooldown > 0) return;

        Vector2 shootAngle = (Vector2)Camera.main.ScreenToWorldPoint(InputController.Instance.PlayerAction.Control.MousePos.ReadValue<Vector2>()) - (Vector2)transform.position;

        ShootData shootData = new ShootData(shootAngle.normalized, Vector2.zero);

        ProjectileManager projectileManager = Instantiate(projectile, transform.position, Quaternion.identity);
        projectileManager.transform.rotation = HelperScript.RotateDirection(shootAngle.normalized, projectileManager.transform, 99999);
        projectileManager.OnSpawn(shootData);

        curCooldown = shootCooldown;
    }

    private void OnDrawGizmos()
    {
        Vector2 shootAngle = (Vector2)Camera.main.ScreenToWorldPoint(InputController.Instance.PlayerAction.Control.MousePos.ReadValue<Vector2>()) - (Vector2)transform.position;

        Gizmos.DrawRay(transform.position, shootAngle.normalized * 10);
    }
}
