using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool SceneIsPaused = false;
    public GameObject AudioManager;

    public GameObject pauseMenuUI;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (SceneIsPaused)
            { Resume(); }
            else { Pause(); }
        }
    }

    public void Resume()
    {
        Debug.Log("Resuming");
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1;
        SceneIsPaused = false;
        AudioManager.SetActive(true);
    }

    public void Pause()
    {
        Debug.Log("Pausing");
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0;
        SceneIsPaused = true;
        AudioManager.SetActive(false);
    }

    public void Menu()
    {
        Time.timeScale = 1;
        SceneIsPaused = false;
        SceneManager.LoadScene("Menu");
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}

