using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HitReciever : MonoBehaviour
{
    [SerializeField] private UnityEvent<AttackData> OnHitEvent;

    public void InvokeOnHit(AttackData attackData)
    {
        OnHitEvent.Invoke(attackData);
    }
}
