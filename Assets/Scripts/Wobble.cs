using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wobble : MonoBehaviour
{
    [SerializeField] private float strenght = 0.5f;
    [SerializeField] private float duration = 1;

    void Start()
    {
        Sequence sequence = DOTween.Sequence().SetLoops(-1,LoopType.Yoyo);
        sequence.Append(transform.DOLocalMoveY(transform.localPosition.y - strenght,duration));
        sequence.Append(transform.DOLocalMoveY(transform.localPosition.y + strenght,duration));


    }


}
