using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

public class ChargerEnemy : State
{
    [SerializeField] private int damage;
    [SerializeField] private bool TrackPlayer;
    [SerializeField] private float speed;

    private void Start()
    {
        if (TrackPlayer)
        {
            transform.rotation = HelperScript.RotateTo(HelperScript.GetPlayer().transform, transform, 9999);
        }
    }

    public override void RunCurrentState()
    {
        transform.Translate(-transform.right * speed * Time.deltaTime);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, transform.position+ -transform.right * 25);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Player") return;

        AttackData attackData = new AttackData(damage);
        collision.transform.GetComponent<Health>().ReduceHealth(attackData);

    }
}
