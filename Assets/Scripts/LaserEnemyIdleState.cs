using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserEnemyIdleState : State
{
    [SerializeField] private int damage;
    [SerializeField] private float laserCooldown;
    [SerializeField] private float laserPreTime;
    [SerializeField] private float laserFreezeTime;
    [SerializeField] private float laserExpandTime;
    [SerializeField] private float rotateSpeed;
    [SerializeField] private GameObject laser;
    [SerializeField] private LayerMask hitMask;
    private Vector2 angleToPlayer;

    private float currentCooldown;
    private bool firing;
    private bool playerHit;

    private bool calculateAngle = true;

    private GameObject target;

    private void Start()
    {
        target = HelperScript.GetPlayer();
        currentCooldown = laserCooldown;
    }

    public override void RunCurrentState()
    {
        if (calculateAngle)
        {
            angleToPlayer = HelperScript.GetAngleToPlayer(transform.position);
            laser.transform.rotation = HelperScript.RotateTo(target.transform, laser.transform, rotateSpeed);
        }

        if (!firing)
        {
            currentCooldown -= Time.deltaTime;
        }
        else
        {
            if (playerHit) return;

            RaycastHit2D hit = Physics2D.Raycast(transform.position,angleToPlayer,99999,hitMask);

            if (!hit) return;

            if (hit.transform.TryGetComponent(out Health health))
            {
                playerHit = true;
                AttackData attackData = new AttackData(damage);
                health.ReduceHealth(attackData);
            }
        }

        if (currentCooldown <= 0)
        {
            ActivateLaser();
            currentCooldown = laserCooldown;
        }
    }

    public void ActivateLaser()
    {
        Sequence sequence = DOTween.Sequence();
        sequence.AppendCallback(() => { laser.transform.DOScaleY(0.1f, 0); }).AppendInterval(laserPreTime); // initiate Laser and wait for laser to aim
        sequence.AppendCallback(() => { calculateAngle = false; }).AppendInterval(laserFreezeTime); // lock the laser
        sequence.Append(laser.transform.DOScaleY(0.5f, laserExpandTime));
        sequence.AppendCallback(() => { firing = true; }).AppendInterval(0.1f);
        sequence.Append(laser.transform.DOScaleY(0.0f, laserExpandTime));
        sequence.AppendCallback(() => { firing = false;  playerHit = false; calculateAngle = true; });
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position,angleToPlayer * 30);
    }
}
