using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalEnemyIdleState : State
{
    [SerializeField] private ProjectileSpawner projectileSpawner;

    public override void RunCurrentState()
    {
        
    }

    public void FireProjectile()
    {
        ShootData shootData = new ShootData(HelperScript.GetAngleToPlayer(transform.position),Vector2.zero);
        projectileSpawner.SpawnProjectile(shootData);
    }
}
