using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDeathState : State
{
    public static Action OnBossDeath;

    public override void RunCurrentState()
    {
     
    }

    public override void OnEnterState()
    {
        gameObject.SetActive(false);
        OnBossDeath?.Invoke();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
