using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MortarEnemyIdleState : State
{
    [SerializeField] private float bulletDamage;
    [SerializeField] private float cooldown;
    [SerializeField] private ProjectileManager mortarBullet;
    private float curCooldown;


    public override void RunCurrentState()
    {
        curCooldown -= Time.deltaTime;

        if(curCooldown <= 0)
        {
            ProjectileManager projectileManager = Instantiate(mortarBullet, transform.position, Quaternion.identity);
            ShootData shootData = new ShootData(Vector2.zero, new Vector2(HelperScript.GetPlayer().transform.position.x,-4));
            projectileManager.OnSpawn(shootData);

            curCooldown = cooldown;
           
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        curCooldown = cooldown;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
