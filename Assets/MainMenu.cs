using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private GameObject knight;
    [SerializeField] private Transform jumpDestination;
    [SerializeField] private SceneLoader sceneLoader;
    private void Start()
    {
        AudioManager.Instance.PlayMusic("MainMenuTheme");

        WaveManager.checkpointEnable = false;
    }

    public void StartGame()
    {

        Sequence sequence = DOTween.Sequence();
        sequence.AppendCallback(() => { canvasGroup.blocksRaycasts = false; });
        sequence.Append(canvasGroup.DOFade(0, 1f)) ;
        sequence.Append(knight.transform.DOJump(jumpDestination.position,10,1,2));
        sequence.AppendCallback(() => { sceneLoader.LoadScene("GameplayScene"); });
    }
}
