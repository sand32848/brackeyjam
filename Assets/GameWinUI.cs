using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameWinUI : MonoBehaviour
{
    [SerializeField] private GameObject gameWinUI;

    private void OnEnable()
    {
        BossDeathState.OnBossDeath += displayVictoryUI;
    }

    private void OnDisable()
    {
        BossDeathState.OnBossDeath -= displayVictoryUI;
    }

    public void displayVictoryUI()
    {
        Sequence sequence = DOTween.Sequence();
        sequence.AppendInterval(0.5f);
        sequence.Append(gameWinUI.GetComponent<RectTransform>().DOAnchorPosY(67, 1f));
    }

}
