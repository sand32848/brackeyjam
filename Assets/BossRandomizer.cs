using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class BossRandomizer : State
{
    [SerializeField] private List<State> stateList;
    private State previousState;
    public override void RunCurrentState()
    {
        int i = Random.Range(0, stateList.Count);

        while (previousState == stateList[i])
        {
            return;
        }

        previousState = stateList[i];
        stateManager.SwitchState(stateList[i]);
    }

    public override void OnEnterState()
    {
       
    }
}
