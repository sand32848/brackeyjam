using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUI : MonoBehaviour
{
    [SerializeField] private List<GameObject> heartList;

    private void OnEnable()
    {
        PlayerHealth.OnPlayerDamage += calculateHeart;
    }

    private void OnDisable()
    {
        PlayerHealth.OnPlayerDamage -= calculateHeart;
    }

    private void calculateHeart(int currentHeart)
    {


        for (int i = 0; i < heartList.Count; i++)
        {

            heartList[i].SetActive(true);
        }

        for (int i = heartList.Count; i > currentHeart; i--)
        {
            heartList[i - 1].SetActive(false);
        }
    }
}
