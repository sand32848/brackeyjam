using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossLaserState : State
{
    [SerializeField] private int damage;
    [SerializeField] private float laserCooldown;
    [SerializeField] private float laserPreTime;
    [SerializeField] private float laserFreezeTime;
    [SerializeField] private float laserExpandTime;
    [SerializeField] private float rotateSpeed;
    [SerializeField] private int amount;

    [SerializeField] private GameObject laserPivot;
    [SerializeField] private List<GameObject> laser;
    [SerializeField] private LayerMask layerMask;
    private Vector2 angleToPlayer;

    private float currentCooldown;
    private bool firing;
    private bool playerHit;
    private int curAmount;

    private bool calculateAngle = true;

    private GameObject target;

    private void Start()
    {
        target = HelperScript.GetPlayer();

        RaycastHit2D hit = Physics2D.Raycast(laser[0].transform.position, HelperScript.GetAngleToPlayer(transform.position), 99999, layerMask);

        if (!hit) return;

        print(hit.transform.gameObject);
        print(hit.transform);

        if (hit.transform.TryGetComponent(out Health health))
        {
            playerHit = true;
            AttackData attackData = new AttackData(damage);
            health.ReduceHealth(attackData);
        }
    }
    public override void RunCurrentState()
    {
        if(curAmount >= amount)
        {
            stateManager.SwitchState(typeof(BossRandomizer));
        }

        if (calculateAngle)
        {
            laserPivot.transform.rotation = HelperScript.RotateTo(target.transform, laserPivot.transform, rotateSpeed);
        }

        if (!firing)
        {
            currentCooldown -= Time.deltaTime;
        }
        else
        {
            if (playerHit) return;

            for (int i = 0; i < laser.Count; i++)
            {
                RaycastHit2D hit = Physics2D.Raycast(laser[i].transform.position, laser[i].transform.right, 9999, layerMask);

                if (!hit) continue;

                print(hit.transform.gameObject);
                print(hit.transform);

                if (hit.transform.TryGetComponent(out Health health))
                {
                    playerHit = true;
                    AttackData attackData = new AttackData(damage);
                    health.ReduceHealth(attackData);
                }
            }
        }

        if (currentCooldown <= 0)
        {
            ActivateLaser();
            currentCooldown = laserCooldown;
        }
    }

    public void ActivateLaser()
    {
        for (int i = 0; i < laser.Count; i++)
        {
            int j = i;

            Sequence sequence = DOTween.Sequence();
            sequence.AppendCallback(() => { laser[j].transform.DOScaleY(0.1f, 0); }).AppendInterval(laserPreTime); // initiate Laser and wait for laser to aim
            sequence.AppendCallback(() => { calculateAngle = false; }).AppendInterval(laserFreezeTime); // lock the laser
            sequence.Append(laser[j].transform.DOScaleY(0.5f, laserExpandTime));
            sequence.AppendCallback(() => { firing = true; }).AppendInterval(0.1f);
            sequence.Append(laser[j].transform.DOScaleY(0.0f, laserExpandTime));
            sequence.AppendCallback(() => { firing = false; playerHit = false; calculateAngle = true; });

        }
        curAmount++;


    }

    public override void OnExitState()
    {
       
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < laser.Count; i++)
        {

            Gizmos.DrawRay(laser[i].transform.position, laser[i].transform.right * 50);
        }

    }
}
