using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerLooker : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    [SerializeField]private bool useScale = false;
    void Start()
    {

        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(HelperScript.GetAngleToPlayer(transform.position).x < 0)
        {
            if (useScale)
            {
                spriteRenderer.transform.localScale = new Vector3(spriteRenderer.transform.localScale.x, spriteRenderer.transform.localScale.y, spriteRenderer.transform.localScale.z);
            }
            else
            {
                spriteRenderer.flipX = false;
            }

        }
        else
        {
            if (useScale)
            {
                spriteRenderer.transform.localScale = new Vector3(-spriteRenderer.transform.localScale.x, spriteRenderer.transform.localScale.y, spriteRenderer.transform.localScale.z);
            }
            else
            {
                spriteRenderer.flipX = true;
            }

        }
    }
}
